﻿#pragma strict

function Start () {

}

function Update () {
	// Arc up
	if (Input.GetKey(KeyCode.UpArrow)) {
		transform.RotateAround(Vector3.zero, Vector3.right, 50 * Time.deltaTime);
	}
	
	// Arc down
	if (Input.GetKey(KeyCode.DownArrow)) {
		transform.RotateAround(Vector3.zero, Vector3.right, -50 * Time.deltaTime);	
	}
	
	// Zoom out
	if (Input.GetKey(KeyCode.KeypadMinus)) {
		transform.Translate(0.0, 0.0, -1.0);
	}
	
	// Zoom in
	if (Input.GetKey(KeyCode.KeypadPlus)) {
		transform.Translate(0.0, 0.0, 1.0);
	}
}